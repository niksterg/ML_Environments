### Extra hints

#### Keep terminal alive

On client-side add these lines to ~/.ssh/config

        Host *
            ServerAliveInterval 120
            ServerAliveCountMax 720

#### Disable visual bell (Wuff!) in screen

Create ~/.screenrc and add the following two lines:

        vbell off
        bell_msg ""

### Show lines correctly in nvtop !

Add this line to ~/.zshrc

        export NCURSES_NO_UTF8_ACS=1

#### Increase file handles for VS Code file watcher 
(in case of error message "Visual Studio Code is unable to watch for file changes in this large workspace")

The current limit can be viewed by running:

        cat /proc/sys/fs/inotify/max_user_watches

The limit can be increased to its maximum by editing /etc/sysctl.conf and adding this line to the end of the file:

        fs.inotify.max_user_watches=524288

The new value can then be loaded in by running 

        sudo sysctl -p

#### Cuda installation

INSTALL NVIDIA drivers v. 5.60
        
        sudo apt install nvidia-driver-560
        reboot
        check status with: lsmod | grep nvidia

2. INSTALL NVIDIA utils

        sudo apt install nvidia-utils-560
        check status with: nvidia-smi

3. INSTALL CUDA 12 (new: it seems that 12.6 installs with nvidia 560 with needing this step ?!)

        wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/cuda-ubuntu2204.pin
        sudo mv cuda-ubuntu2204.pin /etc/apt/preferences.d/cuda-repository-pin-600
        wget https://developer.download.nvidia.com/compute/cuda/12.0.0/local_installers/cuda-repo-ubuntu2204-12-0-local_12.0.0-525.60.13-1_amd64.deb
        sudo dpkg -i cuda-repo-ubuntu2204-12-0-local_12.0.0-525.60.13-1_amd64.deb
        sudo cp /var/cuda-repo-ubuntu2204-12-0-local/cuda-*-keyring.gpg /usr/share/keyrings/
        sudo apt-get update
        sudo apt-get -y install cuda
        (σε περίπτωση που χρεαστεί επανεγκατάσταση της ίδιας cuda, αρκεί η τελευταία εντολή, καθώς το παραπάνω αρχείο υπάρχει στο σύστημα)
4. CREATE CONDA ENVIRONMENT FOR TESTING

        conda create -n torch-gpu python=3.9
        conda activate torch-gpu
        conda install pytorch torchvision torchaudio              pytorch-cuda=11.7 -c pytorch -c nvidia (edited) 


If you need a specific NVIDIA driver version for CUDA compatibility, you might want to hold your kernel version to prevent automatic updates that could break compatibility:

        sudo apt-mark hold linux-generic linux-headers-generic linux-image-generic
        
(Remember to unhold these when you want to update your kernel.)
