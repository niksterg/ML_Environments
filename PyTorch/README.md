### Pytorch conda environment and example notebook

- Download the required data by running the download_data.sh script.

- Create a new conda environment:

        1. conda create --name=pytorch python=3.10
        2. conda activate pytorch
        3. conda install pytorch torchvision torchaudio pytorch-cuda=11.8 -c pytorch -c nvidia
        4. conda install torchmetrics scikit-learn pandas jupyterlab matplotlib

- Next, add the new environment as a kernel for jupyter notebooks

        ipython kernel install --name "pytorch" --user

- To run pytorch codes, always activate the new environment and run jupyter or jupyter lab inside it. Choose "pytorch" as the kernel for the notebook.


