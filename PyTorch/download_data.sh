#!/bin/bash

wget --no-check-certificate -O classification_test_data.npz "https://aristotleuniversity-my.sharepoint.com/:u:/g/personal/niksterg_office365_auth_gr/Ec1eKOLHx3VFpMQCmdf5GXwBq0FrDkN8a9HSsVh-ATaJWg?e=ow9JoU&download=1"
wget --no-check-certificate -O classification_training_data.npz "https://aristotleuniversity-my.sharepoint.com/:u:/g/personal/niksterg_office365_auth_gr/EQyQpfgLZvhDkLpwW4pbOawBzlVN6bj7YhRTmn6qhCZLqA?e=ylL7gf&download=1"
wget --no-check-certificate -O classification_validation_data.npz "https://aristotleuniversity-my.sharepoint.com/:u:/g/personal/niksterg_office365_auth_gr/ERjGiR1PTxBNuEfE9z9KThwB8Yu3YbYM4zk0yaQa-E9aig?e=KCHin7&download=1"

