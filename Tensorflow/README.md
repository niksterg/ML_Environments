### Tensorflow conda environment and example notebook

- Create a new conda environment:

        1. conda create --name=tensorflow python=3.11
		2. conda activate tensorflow
		3. conda install -c conda-forge cudatoolkit=11.8.0 cudnn=8.8.0.121
		4. mkdir -p $CONDA_PREFIX/etc/conda/activate.d
		5. echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/' > $CONDA_PREFIX/etc/conda/activate.d/env_vars.sh
		6. conda deactivate
- Log out of the machine and log in again. Then do

		7. conda activate tensorflow

- Install these additional packages:

		8. python3 -m pip install tensorflow==2.12
		9. conda install keras scikit-learn jupyterlab pandas matplotlib seaborn
		10. pip install chardet

- Next, add the new environment as a kernel for jupyter notebooks

        ipython kernel install --name "tensorflow" --user

- To run tensorflow/keras codes, always activate the new environment and run jupyter or jupyter lab inside it. Choose "tensorflow" as the kernel for the notebook.

