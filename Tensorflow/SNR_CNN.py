import pandas as pd
import numpy as np

# These assignments are needed since numpy 1.24.3 (these deprecated features are no longer supported)
np.object = object
np.bool = bool
np.int = int
np.typeDict = np.sctypeDict

import matplotlib.pylab as plt
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
#from keras.models import Sequential
#from keras.layers import Dense

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense


from sklearn.preprocessing import MaxAbsScaler

#import os
#os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"]="-1"

# To remove the scientific notation from numpy arrays
np.set_printoptions(suppress=True)

# Load the dataset file
dataset = pd.read_pickle('SNR_BBH-injections-reduced-100k.pkl')

dataset = dataset.loc[dataset["SNR"] <= 100]

TargetVariable = ['SNR']
Predictors = ['Mc', 'q', 'inc', 'ra' , 'dec' , 'spin1_mag', 'spin1_polar', 'spin1_azimuthal', 'spin2_mag', 'spin2_polar', 'spin2_azimuthal']


X = dataset[Predictors].values
y = dataset[TargetVariable].values


# Sandardization of data #
PredictorScaler = StandardScaler()
Xscaled = PredictorScaler.fit_transform(X)


TargetScaler =  StandardScaler()
yscaled = TargetScaler.fit_transform(y)

Xscaled = Xscaled.reshape(Xscaled.shape[0], Xscaled.shape[1], 1)

# Split the data into training and testing set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(Xscaled, yscaled, test_size=0.3, random_state=42)

print("Input shape:", X_train.shape)
print("Target shape:", y_train.shape)

from keras.models import Sequential
from keras.layers import Conv1D, MaxPooling1D, Flatten, Dense

model = Sequential()


# Add the first convolutional layer with 32 filters and kernel size of 3, and input shape of (11, 1)
model.add(Conv1D(filters=32, kernel_size=3, activation='relu', padding='same', input_shape=(11, 1)))
model.add(MaxPooling1D(pool_size=2))

# Add the second convolutional layer with 64 filters and kernel size of 3
model.add(Conv1D(filters=64, kernel_size=3, activation='relu', padding='same'))
model.add(MaxPooling1D(pool_size=2))

# Add the third convolutional layer with 128 filters and kernel size of 3
model.add(Conv1D(filters=128, kernel_size=3, activation='relu', padding='same'))
model.add(MaxPooling1D(pool_size=2))

# Flatten the output of the convolutional layers
model.add(Flatten())

# Add a dense layer with 128 units and ReLU activation function
model.add(Dense(units=128, activation='relu'))

# Add an output layer with a single unit and no activation function (regression problem)
model.add(Dense(units=1))



model.compile(loss='mean_squared_error', optimizer='Nadam')

batch_size = 100
epochs = 40


# Fitting the ANN to the Training set
history = model.fit(X_train, y_train, validation_split=0.33, batch_size = batch_size, epochs = epochs, verbose=1)

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'val'], loc='upper right')
plt.savefig("Figure_loss.pdf", format="pdf", bbox_inches="tight")
plt.show()
plt.clf()

# Generating Predictions on testing data
Predictions_scaled = model.predict(X_test)

# Scaling the test data back to original scale

X_test2 = X_test.reshape(X_test.shape[0], X_test.shape[1])
Test_Data = PredictorScaler.inverse_transform(X_test2)

# Scaling the y_test Price data back to original price scale
y_test_orig=TargetScaler.inverse_transform(y_test)

# Scaling the predicted Price data back to original price scale
Predictions = TargetScaler.inverse_transform(Predictions_scaled)

# Create table with test data and predictions
TestingData = pd.DataFrame( data = Test_Data, columns = Predictors)
TestingData['SNR'] = y_test_orig
TestingData['PredictedSNR'] = Predictions

# Computing the absolute percent error
APE = 100*(abs( (TestingData['SNR']-TestingData['PredictedSNR'])/TestingData['SNR']) )
TestingData['APE']=APE

# Computing the error
Error = TestingData['SNR']-TestingData['PredictedSNR']
TestingData['Error']=Error


TestingData.head()

# Compute MAPE and accuracy

MAPE = np.mean(TestingData['APE'])
accuracy = 100 - MAPE

print('MAPE:', MAPE, 'accuracy:', accuracy)

rel_error = 100*(Predictions - y_test_orig)/y_test_orig

count, bins, ignored = plt.hist(rel_error, 2000)
plt.xlabel('% error')
plt.ylabel('count')
plt.xlim(-100,100)

plt.show()
plt.clf()

count, bins, ignored = plt.hist(TestingData['Error'], 1000)
#plt.xlim(-100,100)
plt.xlabel('error')
plt.ylabel('count')

plt.savefig("Figure_error.pdf", format="pdf", bbox_inches="tight")
plt.show()
plt.clf()

count, bins, ignored = plt.hist(y_test_orig, 1000)
count, bins, ignored = plt.hist(Predictions, 1000)
plt.xlabel('SNR')
plt.ylabel('count')

plt.savefig("Figure_SNR.pdf", format="pdf", bbox_inches="tight")
plt.show()
plt.clf()

plt.plot(y_test_orig, Predictions, 'bo')
#plt.ylabel('loss')
#plt.xlabel('epoch')
#plt.legend(['train', 'val'], loc='upper right')
plt.xlabel('SNR')
plt.ylabel('Predicted SNR')

plt.savefig("Figure_predicted.pdf", format="pdf", bbox_inches="tight")
plt.show()
plt.clf()

