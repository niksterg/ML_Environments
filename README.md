# IGWN and ML Conda Environments

## A guide for setting up conda environments for IGWN and Machine Learning

The instructions are for Ubuntu distributions and have been tested for Ubuntu 20.04 LTS and 22.04 LTS.

- Set up a [Conda environment for IGWN](https://gitlab.com/niksterg/ML_Environments/-/tree/main/IGWN).

- Set up a [Conda environment for Tensorflow](https://gitlab.com/niksterg/ML_Environments/-/tree/main/Tensorflow) and test it using an [example notebook](https://gitlab.com/niksterg/ML_Environments/-/blob/main/Tensorflow/SNR_CNN.ipynb).

- Set up a [Conda environment for Pytorch](https://gitlab.com/niksterg/ML_Environments/-/tree/main/PyTorch) and test it using an [example notebook](https://gitlab.com/niksterg/ML_Environments/-/blob/main/PyTorch/G2Net-Hackathon.ipynb).
