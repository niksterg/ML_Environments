### IGWN ENVIRONMENTS

#### Tagged environments

For reproducibility, we are going to use officiall IGWN environments tagged by the date they were 'frozen'. 

#### Exported yaml files for tagged environments

For convenience, this directory contains several tagged IGWN environments that have been exported following the steps shown below:

- [igwn-py311-20241016.yaml](https://gitlab.com/niksterg/ML_Environments/-/blob/main/IGWN/igwn-py311-20241016.yaml)

(Note: this line had to be commented out: _x86_64-microarch-level=3=2_haswell !!)

#### Create environment
An environment can be created e.g. as follows 

        conda env create --name igwn311 --file igwn-py311-20241016.yaml


Up to date (but untagged!) environments for different python versions can be also be found at the [IGWN webpage](https://computing.docs.ligo.org/conda/environments/). 

#### Activate environment
Once created, an environment can be activated e.g. as follows 

        conda activate igwn311


### SET UP CVMFS

Set up [IGWN CVMFS](https://computing.docs.ligo.org/guide/cvmfs/). If the generic instractions fail to launch cvmfs, do these steps:

Create a proper autofs configuration file for CVMFS

        sudo bash -c 'echo "/cvmfs /etc/auto.cvmfs" > /etc/auto.master.d/cvmfs.autofs'

Ensure the CVMFS mount directory exists

        sudo mkdir -p /cvmfs

Double-check your CVMFS configuration

        sudo bash -c 'cat > /etc/cvmfs/default.local << EOF
        CVMFS_REPOSITORIES=software.igwn.org,gwosc.osgstorage.org,oasis.opensciencegrid.org,singularity.opensciencegrid.org
        CVMFS_HTTP_PROXY=DIRECT               
        CVMFS_QUOTA_LIMIT=20000
        CVMFS_CACHE_BASE=/var/lib/cvmfs
        CVMFS_CLAIM_OWNERSHIP=yes
        EOF'

Make sure the cache directory exists and has proper permissions:

        sudo mkdir -p /var/lib/cvmfs
        sudo chmod 777 /var/lib/cvmfs

Restart the autofs service

        sudo systemctl restart autofs

Try probing the repositories again

        cvmfs_config probe


#### Required environment variables for downloading data

To download data with pycbc/gw_datafind, add the following line in ~/.bashrc 

        export GWDATAFIND_SERVER='datafind.gw-openscience.org' 


### How to create exported yaml files for tagged environments

If you want to create a yaml file for a new tagged environment that is not available above, it can be done following these steps:

1. On a linux machine where the [IGWN CVMFS](https://computing.docs.ligo.org/guide/cvmfs/) has been set up, including the `software.igwn.org` server, insert the available remote conda environments with

        source /cvmfs/software.igwn.org/conda/etc/profile.d/conda.sh

2. Probe the available cvmfs repositories

        cvmfs_config probe

3. View the list of available environments with

        ls /cvmfs/software.igwn.org/conda/envs/

4. Activate one of the environments, e.g. 
                
        conda activate igwn-py311-20241016

5. Export the environment to a yaml file
                
        conda env export > ~/igwn-py311-20241016.yaml

(find the exported yaml file in you ~/ directory).

### Older environments

- [igwn-py310-20230425.yaml](https://gitlab.com/niksterg/ML_Environments/-/blob/main/IGWN/igwn-py310-20230425.yaml)
- [igwn-py310-20230615.yaml](https://gitlab.com/niksterg/ML_Environments/-/blob/main/IGWN/igwn-py310-20230615.yaml)

