### MAC

1. Install [Homebrew](https://brew.sh/)
2. Install [Iterm2](https://formulae.brew.sh/cask/iterm2#default)
3. Install or update [Zsch](https://formulae.brew.sh/formula/zsh#default)
4. Install [Oh My Zsh](https://ohmyz.sh/)
5. Install [PowerLevel10k prompt theme](https://github.com/romkatv/powerlevel10k#installation) 

        git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

6. Install the [Meslo Nerd font](https://github.com/romkatv/powerlevel10k/blob/master/font.md)

7. In Iterm2 set the font in Settings -> Profile -> Text -> Font -> MesloLGS NF, Regular, 18. Do not use a different font for ASCII. Set your profile as default.

8. Download [this color scheme file](https://gitlab.com/niksterg/ML_Environments/-/blob/main/Zsch/NS.itermcolors) and import it in Settings -> Profile -> Colors -> Color Presets -> Import. After importing it, you need to select this scheme in Settings -> Profile -> Colors -> Color Presets.

9. Install the following zsh plugins

        git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

        git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

10. In the ~/.zshrc file, edit the "plugins" line to

        plugins=(git zsh-autosuggestions zsh-syntax-highlighting web-search)

11. Load the new plugins by running

        source ~/.zshrc

12. Install [cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html) (needed for LunarVim)

        source "$HOME/.cargo/env"

13. Install [lazygit](https://github.com/jesseduffield/lazygit) (needed for LunarVim)

        brew install jesseduffield/lazygit/lazygit

14. Install [Neovim](https://neovim.io/)

15. Install [LunarVim](https://www.lunarvim.org/)

16. Install plugin manager [vim-plug](https://github.com/junegunn/vim-plug) for vim

In remote ubuntu shell need **export TERM=screen-256color** for nvim color schemes to work ok.

17. Mount remote file system with ssfs-mac, see [here](https://code.visualstudio.com/docs/remote/troubleshooting#_installing-a-supported-ssh-server) and [here](https://www.petergirnus.com/blog/how-to-use-sshfs-on-macos) (use the mount example from the second link)

        brew install --cask macfuse
        brew install gromgit/fuse/sshfs-mac
        brew link --overwrite sshfs-mac

        export USER_AT_HOST=user@hostname

        # Make the directory where the remote filesystem   will be mounted
        mkdir -p "$HOME/sshfs/$USER_AT_HOST"

        # Mount the remote filesystem
        sshfs user@host:/home/user $HOME/sshfs/$USER_AT_HOST




### UBUNTU

1. Install or update zsh, if not available 

        sudo apt install zsh

2. Install [Oh My Zsh](https://ohmyz.sh/)

3. Install the [Meslo fonts](https://gist.github.com/incogbyte/373f37817742c53891a076391533fe6d)

        sudo apt install fontconfig
        cd ~
        wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Meslo.zip
        mkdir -p .local/share/fonts
        unzip Meslo.zip -d .local/share/fonts
        cd .local/share/fonts
        rm *Windows*
        cd ~
        rm Meslo.zip
        fc-cache -fv

4. Install [PowerLevel10k prompt theme](https://github.com/romkatv/powerlevel10k#installation) 

        git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

5. Edit the file ~/.zshrc and change the ZSH_THEME variable to:

        ZSH_THEME="powerlevel10k/powerlevel10k"

6. Log out and log in again. This should trigger the p10k configuration. Otherwise, run 

        p10k configure

7. Install the following zsh plugins

        git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

        git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

8. In the ~/.zshrc file, edit the "plugins" line to

        plugins=(git zsh-autosuggestions zsh-syntax-highlighting web-search)

9. Load the new plugins by running

        source ~/.zshrc

### POST-INSTALL:

1. Enable wildcards by addind this line to ~/.zshrc

        # enable wildcards
        unsetopt nomatch





